import numpy as np

from preprocessing import DataPreparation
import prediction


def calculateScore(feat_a, feat_b):
    return np.sum(np.abs(feat_a - feat_b))


"""
    ToDo:
        - take only the highest value for comparing to multiple train
            images of the same whale
"""
if __name__ == "__main__":

    dataHandler = DataPreparation(target_size=(400, 400))

    print("Loading training data")
    trainingData = dataHandler.get_trainset(maxEntries=1000)
    print("Loading testing data")
    testingData = dataHandler.get_test_data(maxEntries=200)

    predictions = []
    testImgs = []

    for testImgName in testingData:
        testImg = testingData[testImgName]
        scores = []
        whaleIDs = []
        for trainImgName in trainingData:
            trainImg = trainingData[trainImgName]
            score = calculateScore(testImg["array"], trainImg["array"])

            whaleID = trainImg["whale_id"]
            # take only highes score for the same train whale
            if whaleID in whaleIDs:
                ix = whaleIDs.index(whaleID)
                if score > scores[ix]:
                    scores[ix] = score
            else:
                whaleIDs.append(whaleID)
                scores.append(score)

            # print(testImgName, trainImgName, score, whaleID)

        scoreDict = {}
        for whale, score in zip(whaleIDs, scores):
            scoreDict[whale] = score

        # scoreD = np.array([whaleIDs, scores]).T

        pred_whaleIDs, pred_scores = prediction.generate_single_prediction(scoreDict)

        predictions.append(pred_whaleIDs)
        testImgs.append(testImgName)

        print(pred_whaleIDs)

    print(testImgs)
    print(predictions)
    prediction.writeSubmissionFile(testImgs, predictions)
