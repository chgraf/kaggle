"""
Builds prediction in the format of [A,B,C,D,E] with A-E being whale IDs.
Adding newWhale at a fixed threshold.
"""

import numpy as np
import csv

from operator import itemgetter


def generate_single_prediction(scoreDict, newWhaleThreshold=0.1, reverseSort=False):
    """
        Generates prediction: vector of whaleIDs of length <sizeOfPrediction>
        that have lowest / highest (reverseSort=True) score. newWhale is added
        at <newWhaleTheshold>

        Input: dict
            [whaleID <str>: score <float>]

        Returns:
            - numpy array with list of predicted whaleIDs
            - numpy array with corresponding scores
    """

    sizeOfPrediction = 5  # length of output vector

    # Add newWhale to the list
    scoreDict["new_whale"] = newWhaleThreshold

    # sort by second column
    scoreDict = sorted(scoreDict.items(), key=itemgetter(1), reverse=reverseSort)

    whaleIDs = [x[0] for x in scoreDict]
    scores = [x[1] for x in scoreDict]

    result_whale = np.array(whaleIDs[:sizeOfPrediction])
    result_score = np.array(scores[:sizeOfPrediction])

    return result_whale, result_score


def writeSubmissionFile(testImgs, predictions, fileName="submission.csv"):
    """
        writes the submission file as a csv file in the format
        testImgName, prediction_1 prediction_2 prediction_3 prediction_4 prediction_5

        Input: takes list of test images and list of predictions as input
    """
    with open(fileName, mode="w") as submission_file:
        submission_writer = csv.writer(submission_file, delimiter=",")
        for img, prediction in zip(testImgs, predictions):
            predictionStr = ""
            for e in prediction:
                predictionStr += str(e) + " "
            submission_writer.writerow([str(img), predictionStr])


if __name__ == "__main__":
    N = 20
    test_scores = np.random.rand(N)
    test_whaleIDs = np.linspace(0, N - 1, N).astype(str)

    print("\nTest scores:")
    print(test_scores)

    print("\nTest whaleIDs:")
    print(test_whaleIDs)

    test_input = {}
    for i in range(0, N):
        test_input[test_whaleIDs[i]] = float(test_scores[i])

    whales, scores = generate_single_prediction(
        test_input, newWhaleThreshold=0.1, reverseSort=False
    )

    print("\n------ Output ------")
    print("\nPredicted whaleIDs:")
    print(whales)
    print("\nCorresponding scores:")
    print(scores)
