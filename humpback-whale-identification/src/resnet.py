"""
Imports ResNet50 pre-trained weights in order to
predict the 512 feature vector for each whale image
"""


from keras.applications.resnet50 import ResNet50
from keras.models import Model
from keras.layers import Conv2D, GlobalMaxPooling2D
import numpy as np


def create_resnet(input_shape):
    """
    Return the model object of a pretrained ResNet50 model
    """
    resnet = ResNet50(include_top=False, input_shape=input_shape, weights="imagenet")

    resnet_output = resnet.output  # 13x13x2048
    x = Conv2D(512, (3, 3), activation="relu", name="conv_512")(
        resnet_output
    )  # 11x11x512
    x = GlobalMaxPooling2D(name="output_pooling")(x)  # 512

    # features = Dense(512, activation="sigmoid", name="output_features_1")(x)

    model = Model(inputs=resnet.input, outputs=x)

    return model


def predict_features(img_array, resnet):
    """
    Return the 512 predicted feature vector from the
    pretrained ResNet50 model
    """

    # reshape the input image to be in a batch of size 1
    img_array = np.reshape(img_array, (1,) + img_array.shape)

    vector = resnet.predict(img_array)

    # return the un-batched shape
    return np.reshape(vector, (512,))


if __name__ == "__main__":
    test_img = np.random.rand(400, 400, 3)
    test = predict_features(test_img)
    print(test.shape)
