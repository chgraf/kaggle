#! /usr/bin/env python
"""
-------------------------------------------------------
 File: siamese_cnn.py
 Author: Blake Forland (bforland@indiana.edu)
 Created: 2019-01-29

 Description:
 This is an incomplete toy implementation of a siamese net.

 Usage:
 pipenv run python siamese_cnn.py

-------------------------------------------------------
"""
import time

import numpy

import keras.backend as K

from keras.layers.core import Dense

# from keras.layers.core import Dropout
from keras import regularizers
from keras.callbacks import EarlyStopping
from keras.layers.core import Flatten, Conv2D, MaxPooling2D, Input
from keras.models import Model
from keras.optimizers import Adam

from sklearn.preprocessing import RobustScaler

from tensorflow import set_random_seed

numpy.random.seed(1)
set_random_seed(2)
TARGET_SIZE = (400, 400)


def scale(array):

    transformer = RobustScaler().fit(array)

    return transformer.transform(array)


def customLoss(y_true, y_pred):
    # ahoenle: Just added this alternative version, as the original (commented out below)
    #          would not work (uses different args than it expects)
    return K.sum(y_true * K.log(y_true) - K.log(y_pred))


# def customLoss(yTrue_1, yPred_1, yTrue_2, yPred_2):
#     return K.sum(yTrue * K.log(yTrue) - K.log(yPred))


def distance_metric(input_1, input_2):
    return K.abs(K.sum(input_1 - input_2))


def siamese_model():

    # initialize train data generator
    batch_size = 32
    # ahoenle: added random values here, needs to be optimised
    epochs = 50
    validation_split = 0.8
    save_model = False
    # data_prep = DataPreparation(get_project_dir(), target_size=TARGET_SIZE)
    # train_gen = data_prep.train_data_generator(batch_size)

    """
    Branch 1
    """

    cnn_1_input = Input(shape=(400, 400, 1), name="cnn_1_input")

    cnn_1_x = Conv2D(
        64,
        (40, 40),
        padding="valid",
        kernel_initializer="he_uniform",
        kernel_regularizer=regularizers.l2(0.01),
        activation="relu",
    )(cnn_1_input)
    cnn_1_x = MaxPooling2D(pool_size=(2, 2), padding="valid", strides=(1, 1))(cnn_1_x)
    # cnn_1_x = Dropout(.3)(cnn_1_x)
    cnn_1_x = Conv2D(
        128,
        (28, 28),
        padding="valid",
        kernel_initializer="he_uniform",
        kernel_regularizer=regularizers.l2(0.01),
        activation="relu",
    )(cnn_1_x)
    cnn_1_x = MaxPooling2D(pool_size=(2, 2), padding="valid", strides=(1, 1))(cnn_1_x)
    # cnn_1_x = Dropout(.3)(cnn_1_x)
    cnn_1_x = Conv2D(
        128,
        (16, 16),
        padding="valid",
        kernel_initializer="he_uniform",
        kernel_regularizer=regularizers.l2(0.01),
        activation="relu",
    )(cnn_1_x)
    cnn_1_x = MaxPooling2D(pool_size=(2, 2), padding="valid", strides=(1, 1))(cnn_1_x)
    # cnn_1_x = Dropout(.3)(cnn_1_x)
    cnn_1_x = Conv2D(
        256,
        (16, 16),
        padding="valid",
        kernel_initializer="he_uniform",
        kernel_regularizer=regularizers.l2(0.01),
        activation="relu",
    )(cnn_1_x)
    cnn_1_x = MaxPooling2D(pool_size=(2, 2), padding="valid", strides=(1, 1))(cnn_1_x)
    # cnn_1_x = Dropout(.3)(cnn_1_x)

    cnn_1_x = Flatten()(cnn_1_x)

    # cnn_1_x = Dense(516, activation='relu')(cnn_1_x)

    """
    Branch 2
    """

    cnn_2_input = Input(shape=(400, 400, 1), name="cnn_2_input")

    cnn_2_x = Conv2D(
        64,
        (40, 40),
        padding="valid",
        kernel_initializer="he_uniform",
        kernel_regularizer=regularizers.l2(0.01),
        activation="relu",
    )(cnn_2_input)
    cnn_2_x = MaxPooling2D(pool_size=(2, 2), padding="valid", strides=(1, 1))(cnn_2_x)
    # cnn_2_x = Dropout(.3)(cnn_2_x)
    cnn_2_x = Conv2D(
        128,
        (28, 28),
        padding="valid",
        kernel_initializer="he_uniform",
        kernel_regularizer=regularizers.l2(0.01),
        activation="relu",
    )(cnn_2_x)
    cnn_2_x = MaxPooling2D(pool_size=(2, 2), padding="valid", strides=(1, 1))(cnn_2_x)
    # cnn_2_x = Dropout(.3)(cnn_2_x)
    cnn_2_x = Conv2D(
        128,
        (16, 16),
        padding="valid",
        kernel_initializer="he_uniform",
        kernel_regularizer=regularizers.l2(0.01),
        activation="relu",
    )(cnn_2_x)
    cnn_2_x = MaxPooling2D(pool_size=(2, 2), padding="valid", strides=(1, 1))(cnn_2_x)
    # cnn_2_x = Dropout(.3)(cnn_2_x)
    cnn_2_x = Conv2D(
        256,
        (16, 16),
        padding="valid",
        kernel_initializer="he_uniform",
        kernel_regularizer=regularizers.l2(0.01),
        activation="relu",
    )(cnn_2_x)
    cnn_2_x = MaxPooling2D(pool_size=(2, 2), padding="valid", strides=(1, 1))(cnn_2_x)
    # cnn_2_x = Dropout(.3)(cnn_2_x)

    cnn_2_x = Flatten()(cnn_2_x)

    # cnn_2_x = Dense(516, activation='relu')(cnn_2_x)

    distance = distance_metric(cnn_1_x, cnn_2_x)

    """
    Final Output
    """

    final_output = Dense(1, activation="sigmoid", name="main_output")(distance)

    """
    Everything blow this point is incomplete at this point
    """

    model = Model(inputs=[cnn_1_input, cnn_2_input], outputs=final_output)
    # model2 = Model(inputs=main_input, outputs=lund_out)

    # opt = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, decay=0.001)
    opt = Adam(beta_1=0.9, beta_2=0.999, decay=0.001)

    model.compile(optimizer=opt, loss=customLoss, metrics=["accuracy"])

    earlystop = EarlyStopping(
        monitor="val_loss", min_delta=0.00001, patience=5, verbose=1, mode="auto"
    )

    callbacks_list = [earlystop]

    t1 = time.time()

    print(model.summary())

    # plot_model(model, to_file='cnn_dnn_api.png')

    # TODO: Fill me!
    x_train_1 = None
    x_train_2 = None
    y_train_1 = None
    y_train_2 = None
    x_test_1 = None
    x_test_2 = None
    data_holder = None

    history = model.fit(
        [x_train_1, x_train_2],
        [y_train_1, y_train_2],
        epochs=epochs,
        batch_size=batch_size,
        shuffle=False,
        validation_split=validation_split,
        sample_weight=data_holder.w_train,
        callbacks=callbacks_list,
    )

    if save_model:
        model.save("simple_cnn.h5")

    y_pred = model.predict([data_holder.x_test])

    score = model.evaluate(
        [x_test_1, x_test_2], data_holder.y_test, batch_size=batch_size
    )
    print(score)
    print(" ")
    runtime = time.time() - t1
    print("Runtime was " + str(runtime))
    loss = history.history["loss"]
    val_loss = history.history["val_loss"]
    acc = history.history["acc"]
    val_acc = history.history["val_acc"]

    numpy.savez_compressed(
        "results_" + str(numpy.round(time.time(), 0)).split(".")[0] + ".npz",
        x_test=data_holder.x_test,
        y_test=data_holder.y_test,
        y_pred=y_pred,
        train_loss=loss,
        train_acc=acc,
        val_loss=val_loss,
        val_acc=val_acc,
        # params=vars(params),
        config=model.get_config(),
    )


# -------------------------------------------------------
# Main:
# -------------------------------------------------------
if __name__ == "__main__":
    siamese_model()
