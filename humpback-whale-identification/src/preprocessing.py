"""
preprocessing.py
Created on: 12/15/2018
Prepare and process competition data for model fitting
"""

import os
import pickle
import shutil
import numpy as np
import csv
import random
from collections import defaultdict, deque

# import matplotlib.pyplot as plt
from PIL import Image
from albumentations import (
    ShiftScaleRotate,
    OpticalDistortion,
    GridDistortion,
    IAAPiecewiseAffine,
    Flip,
    OneOf,
    Compose,
)

from utils import get_project_dir
from resnet import predict_features, create_resnet


class DataPreparation:
    """Class that prepares and generates the competition data for
       model fitting
    """

    def __init__(self, base_dir=None, target_size=(350, 350)):
        """Set the full base directory path and train/test paths of the repo.
        The repo is currently expected to have the folder structure:

        - humpback-whale-identification/
            - src/
            - data/
                - sample_submission.csv
                - train.csv
                - test/
                    - all .jpg test images
                - train/
                    - all .jpg train images
        """

        if base_dir is None:
            self.BASEDIR = get_project_dir()
        else:
            self.BASEDIR = base_dir

        self.DATADIR = os.path.join(self.BASEDIR, "data")
        self.TRAINDIR = os.path.join(self.BASEDIR, "data", "train")
        self.TESTDIR = os.path.join(self.BASEDIR, "data", "test")

        self.TARGET_SIZE = target_size

        self.SEED = 10
        random.seed(self.SEED)
        np.random.seed(self.SEED)

    def create_subsets(self, n):
        """Subsets the data equally to n parts for collaborative whale image cleaning"""
        train_fnames = os.listdir(self.TRAINDIR)

        # use numpy to split img names array equally
        subsetted = np.array_split(train_fnames, n)

        # copy subsets to separate folders at base of repo for later file transfer
        for i, subset in enumerate(subsetted):
            subset_path = os.path.join(self.BASEDIR, os.path.join("data/", str(i)))
            os.mkdir(subset_path)
            for image_name in subset:
                image_path = os.path.join(self.TRAINDIR, image_name)
                shutil.copy2(image_path, subset_path)

            # print out subset size
            print(len(os.listdir(subset_path)))

    def image_aug(self, p=0.5):
        """
        Initializes augmentation functions to apply to image array
        More info on this very YOUNG but powerful python package
        can be found here: https://github.com/albu/albumentations

        Args:
            p:  double less than 1, controls how likely any number of the
            listed transformations will be applied"""

        # all arbitrary values currently
        return Compose(
            [
                Flip(),
                ShiftScaleRotate(
                    shift_limit=0.08, scale_limit=0.2, rotate_limit=60, p=0.7
                ),
                OneOf(
                    [
                        OpticalDistortion(p=0.3),
                        GridDistortion(p=0.1),
                        IAAPiecewiseAffine(p=0.3),
                    ],
                    p=0.2,
                ),
            ],
            p=p,
        )

    def read_image(self, im_path, p=0.8, validation=False, show_fig=False):
        """
        Apply random image transformations if for training data
        and return an image in the form of a numpy array
        """

        # read in image file, resize, and convert to array
        image = Image.open(im_path)
        # image = image.convert("L")  # convert to greyscale

        image = image.resize(
            self.TARGET_SIZE, Image.ANTIALIAS
        )  # ANTIALIAS resizes with best quality
        image = np.array(image)

        # skip augmentation
        if validation:
            # reshape to 4D array and normalize
            try:
                image = np.reshape(image, self.TARGET_SIZE + (3,)) / 255
            except ValueError:
                image = np.stack((image,) * 3, axis=-1)

            return image

        # initialize image augmentation class
        augmentation = self.image_aug(p=p)

        augmented_img = augmentation(image=image, p=p)["image"]

        # if show_fig:
        # plt.figure(figsize=(10, 10))
        # plt.imshow(augmented_img)

        # reshape array to 4D array and normalize color values in range [0, 255]
        try:
            # this will fail if the image only has one color channel
            augmented_img = np.reshape(augmented_img, self.TARGET_SIZE + (3,)) / 255
        except ValueError:
            # duplicate the black white channel three times
            # to create a 4D shape
            augmented_img = np.stack((augmented_img,) * 3, axis=-1)

        return augmented_img

    def get_whale_groups(self):
        """
        Return two dictionaries of {whale: [images]} with only whitelisted images
        and whale IDs with > 1 images associated with that ID.
        """

        # load in bad whale image names
        blacklistf = open(os.path.join(self.DATADIR, "blacklist.txt"), "r")
        blacklist = blacklistf.readlines()
        blacklistf.close()

        # initialize a dict with structure {key : list()}
        grouped = defaultdict(list)
        whale_counter = 0

        # this solution found on SO:
        # https://stackoverflow.com/questions/25055958/group-data-from-a-csv-file-by-field-value
        with open(os.path.join(self.DATADIR, "train.csv"), "r") as trainf:
            csvreader = csv.reader(trainf, delimiter=",")
            for row in csvreader:
                # row[0] is the img name and row[1] is the whale id
                if row[0] not in blacklist and row[1] != "new_whale":
                    grouped[row[1]].append(row[0])
                    whale_counter += 1

        self.number_same_pairs = whale_counter // 2

        # get only whale ids with more than 1 image name
        grouped = {whale: imgs for (whale, imgs) in grouped.items() if len(imgs) > 1}

        group_b = grouped.copy()

        # derange the whale image names of each whale id by
        # shifting the image name indexes by one
        for whale_id, img_names in group_b.items():
            shifted = deque(img_names)
            shifted.rotate(1)
            group_b[whale_id] = list(shifted)

        return grouped, group_b

    def get_trainset(self, maxEntries):
        """
        Convert jpg images to array and apply a transform. Pickle them into
        a referencable dictionary with structure:
        { "img_name": {"whale_id":str, "array":np.array(), "img_id":int} }
        Pickle file is written at the base of the data directory.
        """

        datafiles = os.listdir(self.DATADIR)
        if "trainset.p" in datafiles:
            pickle_path = os.path.join(self.DATADIR, "trainset.p")
            trainset = pickle.load(open(pickle_path, "rb"))
            return trainset

        whales_grouped, _ = self.get_whale_groups()

        dump = {}
        img_id = 0

        # initialize the ResNet50 model
        resnet = create_resnet((self.TARGET_SIZE + (3,)))

        for whale_id, img_names in whales_grouped.items():
            for name in img_names:
                if name not in dump.keys():
                    dump[name] = {}
                    img_path = os.path.join(self.TRAINDIR, name)
                    array = self.read_image(img_path)

                    # predict features from resnet
                    array = predict_features(array, resnet)

                    dump[name]["whale_id"] = whale_id
                    dump[name]["array"] = array
                    dump[name]["img_id"] = img_id

                    # show progress during generation
                    print(img_id)

                    img_id += 1

            if img_id > maxEntries:
                break

        pickle.dump(dump, open(os.path.join(self.DATADIR, "trainset.p"), "wb"))
        return dump

    def get_train_data(self):
        """
        Create train data for model fitting.
        Dataset has row structure [img_a, img_b, score] with a
        score of 1 representing same whale or 0 representing different whale.
        """

        group_a, group_b = self.get_whale_groups()
        trainset = self.get_trainset()

        matched = []
        unmatched = []

        # horizontally grab matched image names from whale groups
        for id_a, id_b in zip(group_a, group_b):
            for name_a, name_b in zip(group_a[id_a], group_b[id_b]):
                # create data row of [image_a, image_b, score]
                if trainset[name_a]["img_id"] != trainset[name_b]["img_id"]:
                    image_a = trainset[name_a]["array"]
                    image_b = trainset[name_b]["array"]
                    row = [image_a, image_b, 1.0]
                    matched.append(row)

        matched = np.array(matched)
        print("Matches shape:", matched.shape)

        # randomly select image names of different whale ids
        image_names = list(trainset.keys())

        i = 0  # have equally as many unmatched whales as matched
        while i < matched.shape[0]:
            random_names = random.sample(image_names, 2)
            a = random_names[0]
            b = random_names[1]
            # add data row if of different whale ids
            if trainset[a]["whale_id"] != trainset[b]["whale_id"]:
                row = [trainset[a]["array"], trainset[b]["array"], 0.0]
                unmatched.append(row)
                i += 1

        unmatched = np.array(unmatched)
        print("Unmatched shape:", unmatched.shape)

        # combine the pairs and shuffle
        all_data = np.concatenate((matched, unmatched), axis=0)
        np.random.shuffle(all_data)
        print("All data:", all_data.shape)

        return all_data

    def get_test_data(self, maxEntries):
        """
        Returns a dictionary with key structure:
        { "img_name": {"whale_id":str, "array":np.array(), "img_id":int} }
        of the testing images and dumps to a pickle file if 'testset.p' doesn't exist
        """

        datafiles = os.listdir(self.DATADIR)
        if "testset.p" in datafiles:
            pickle_path = os.path.join(self.DATADIR, "testset.p")
            testset = pickle.load(open(pickle_path, "rb"))
            return testset

        test_names = os.listdir(self.TESTDIR)
        dump = {}
        resnet = create_resnet((self.TARGET_SIZE + (3,)))

        for i, name in enumerate(test_names):
            dump[name] = {}

            img_path = os.path.join(self.TESTDIR, name)

            # do not apply image transforms
            features = self.read_image(img_path, validation=True)

            # predict features from resnet
            features = predict_features(features, resnet)

            dump[name]["array"] = features
            dump[name]["img_id"] = i
            # whale ID not yet predicted at this point
            dump[name]["whale_id"] = str()

            print(i)
            if i > maxEntries:
                break

        pickle.dump(dump, open(os.path.join(self.DATADIR, "testset.p"), "wb"))
        return dump


if __name__ == "__main__":
    preprocessing = DataPreparation(target_size=(400, 400))
    preprocessing.get_test_data()
