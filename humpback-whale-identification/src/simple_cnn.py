"""
First order approach to creating a model using Keras
"""

import os
from keras import layers, models
from keras.losses import categorical_crossentropy
import numpy as np
from PIL import Image

from preprocessing import DataPreparation

from utils import get_project_dir

TARGET_SIZE = (400, 400)


def cnn_model(save_model=False):
    """Build and train simple CNN"""

    # initialize train data generator
    batch_size = 32
    data_prep = DataPreparation(get_project_dir(), target_size=TARGET_SIZE)
    train_gen = data_prep.train_data_generator(batch_size)

    # build model structure
    model = models.Sequential()

    model.add(
        layers.Conv2D(120, (5, 5), activation="tanh", input_shape=TARGET_SIZE + (1,))
    )

    model.add(layers.MaxPooling2D((3, 3)))
    model.add(layers.Conv2D(132, (5, 5), activation="sigmoid"))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(132, (5, 5), activation="relu"))
    model.add(layers.Conv2D(132, (5, 5), activation="tanh"))
    model.add(layers.MaxPooling2D((3, 3)))
    model.add(layers.Flatten())
    model.add(layers.Dense(512, activation="relu"))
    # model returns array of size (5005, )
    model.add(layers.Dense(5005, activation="softmax"))

    model.summary()

    model.compile(loss=categorical_crossentropy, optimizer="rmsprop")

    # calculate number of times to call generator before ending an epoch
    total_images = len(os.listdir(data_prep.TRAINDIR)) - 2  # discludes supporting files
    steps = total_images // batch_size

    model.fit_generator(train_gen, steps_per_epoch=steps, epochs=10)

    if save_model:
        model.save("simple_cnn.h5")

    test_impath = os.path.join(data_prep.TESTDIR, "0a2a6904f.jpg")
    test_image = Image.open(test_impath)
    test_image = test_image.convert("L")  # convert to greyscale

    test_image = test_image.resize(
        TARGET_SIZE, Image.ANTIALIAS  # ANTIALIAS resizes with best quality
    )
    test_image = np.array(test_image)

    # reshape and normalize test image array
    # model.predict() expects array of size (batch_size, img_size, 1)
    test_image = np.reshape(test_image, (1,) + test_image.shape + (1,)) / 255

    return model.predict(test_image)


if __name__ == "__main__":
    cnn_model()
