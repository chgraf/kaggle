Kaggle Challanges
=================

Repository of kaggle challanges I participate in. Some of them were performed within the Kagglenauts
team (Andreas, Blake, Michael, and me).

* Humpback whale identification (computer vision): with Kaglenauts team. The goal for us was to get
  in touch with Kaggle challanges and try to reimplement and understand a previous solution based
  on a siamese neural network 

* Gender pronoun resolution (NLP): with Kaglenauts team. The goal for us was to
  actively follow the challange and to learn together and try out first steps in the field of NLP.

* Abstraction and Reasoning: solo. My goal was to actively participate in the challange, follow the
  discussions and learn as much as possible in a very difficult and open challange.
