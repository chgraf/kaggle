This is the common data directory.

Please access files here with

```python
from utils import get_project_dir

my_file = os.path.join(get_project_dir(), "file1.foo")
```

**Never** use hard-coded paths!


### Files for the gendered pronoun resolution project

#### sample\_submission\_stage\_1

Some data for R&D in stage 1 of the challenge.

#### sample\_submission\_stage\_1

A sample submission file to give you an idea how the submission looks like.