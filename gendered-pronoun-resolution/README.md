[![pipeline status](https://gitlab.com/kagglenauts/humpback-whale-identification/badges/master/pipeline.svg)](https://gitlab.com/kagglenauts/humpback-whale-identification/commits/master)

## Setup your workspace

1. Follow the instructions [here](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair) to generate a SSH key

2. Clone repository

    ```bash
    git clone --recursive git@gitlab.com:kagglenauts/gendered-pronoun-resolution.git
    ```
    If you followed the instructions in 1. and successfully added the key to your ssh config, this will work without being prompted for a username/PW.

3. Make sure `pre-commit` is installed
    On Mac:

    ```bash
    brew install pre-commit
    ```

    On Linux

    ```bash
    curl https://bootstrap.pypa.io/get-pip.py | sudo python - pre-commit
    ```

4. Install pre-commit hooks

    Execute this command in your project directory
    ```bash
    pre-commit install
    ```

5. Setup your python environment

    We use the [pyenv](https://github.com/pyenv/pyenv)/[pipenv](https://github.com/pypa/pipenv) combination to manage requirements.
    I've only used this with MacOS so far.
    Open an Issue if there are any problems on Linux with this proposal.

    Everything we need is defined in the Pipfile and all you have to do to get your environment is

    ```bash
    pipenv shell --python 3.6.6
    ```

    (This of course must be executed in the project directory.)

    **Note:** Both `Pipfile` and `Pipfile.lock` should be included in your commit if you install new packages.

## Running notebooks

Your pipenv will have jupyter installed. Start it like this

```bash
cd notebooks
jupyter notebook
```

Now you can connect to `http://127.0.0.1:8888` in your browser.
Jupyter will create a token that allows for a login without credentials.

```bash
[C 13:14:19.033 NotebookApp]

    Copy/paste this URL into your browser when you connect for the first time,
    to login with a token:
        http://(idefix or 127.0.0.1):8888/?token=a591e083039bcbda69c26ff668863b1d44bdf99eec212b49
```

Make sure to use `http://127.0.0.1:8888?token=xxx` when you first connect.

## Development workflow

Our `master` branch is protected.
After you cloned the repository you should create a devbranch.
One branch per idea/feature/etc. is recommended, i.e.

```bash
git checkout -b andi-add-dataprocessing
```

You commit to this branch.
Use [meaningful](https://chris.beams.io/posts/git-commit/) commit messages.

When you're done push your code

```bash
git push -u origin andi-add-dataprocessing
```

and open a [merge request](https://gitlab.com/kagglenauts/humpback-whale-identification/merge_requests).

Do **not** merge new code yourself.
We want to have a proper code review before merging.
Exceptions are commits that target only your personal dev notebooks.

Always check _Delete source branch after merging_.
We don't want to clutter up our repo with old branches.

## A comment on pre-commit hooks

The hooks will run code formatting and style checks.
They use [black](https://pypi.org/project/black/) and [flake8](http://flake8.pycqa.org/en/latest/).
If either command finds a reason to complain, the commit will be aborted.
You have to add the modified files and commit again.

Example:

```bash
git add test.py
git commit -m 'Add test.py'  # let's assume black will complain about this file
# pre-commit hooks will be executed here, because of black, commit will be aborted
git add test.py  # this adds the _updated_ version of test.py, including the changes made by black
git commit -m 'Add test.py'
# pre-commit hooks will succeed
# Done.
```

The tools run with default configurations.
If you think we should change that or exclude some files from the checks, open an [Issue](https://gitlab.com/kagglenauts/humpback-whale-identification/issues).

## Code formatting tools in your Editor

I recently started using [Visual Studio Code](https://code.visualstudio.com/) and I can highly recommend it.
I've been an Emacs enthusiast for years, and VS Code is the first editor that convinced me.

In VS Code you can install both black and flake8 formatting tools and with a simple keyboard shortcut run the formatting.
The huge advantage is that VS Code is aware of the virtual environments on your machine.
In other words, you can have different versions/rules for different workspaces.

## Known problems

### pipenv install fails

In my setup `pipenv install <package>` often fails with an error like this

```
Adding kaggle to Pipfile's [packages]...
Pipfile.lock not found, creating...
Locking [dev-packages] dependencies...
Locking [packages] dependencies...
lib/python3.7/site-packages/pipenv/utils.py", line 402, in resolve_deps
    req_dir=req_dir
  File "/usr/local/Cellar/pipenv/2018.7.1/libexec/lib/python3.7/site-packages/pipenv/utils.py", line 250, in actually_resolve_deps
    req = Requirement.from_line(dep)
  File "/usr/local/Cellar/pipenv/2018.7.1/libexec/lib/python3.7/site-packages/pipenv/vendor/requirementslib/models/requirements.py", line 704, in from_line
    line, extras = _strip_extras(line)
TypeError: 'module' object is not callable
```

The problem was introduced by pip v18.1 and can be fixed by going back to pip v18.0 with

```bash
pip install pip==18.0
```

This one should also do the trick (upgrading pipenv)
```bash
brew upgrade
```
