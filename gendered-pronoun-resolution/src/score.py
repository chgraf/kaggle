import os

import numpy as np
import pandas as pd

from utils import get_project_dir


def rescaleRows(df):
    """
        From Kaggle:
        The submitted probabilities are not required to sum to one
        because they are rescaled prior to being scored
        (each row is divided by the row sum).
    """

    rowSums = df["A"] + df["B"] + df["NEITHER"]

    df["A"] = df["A"] / rowSums
    df["B"] = df["B"] / rowSums
    df["NEITHER"] = df["NEITHER"] / rowSums

    return df


def replaceExtremes(df):
    """
        From Kaggle:
        In order to avoid the extremes of the log function, predicted
        probabilities are replaced with max(min(p,1−10−15),10−15).
    """

    df["A"] = np.maximum(np.minimum(df["A"], 1.0 - 10e-15), 10e-15)
    df["B"] = np.maximum(np.minimum(df["B"], 1.0 - 10e-15), 10e-15)
    df["NEITHER"] = np.maximum(np.minimum(df["NEITHER"], 1.0 - 10e-15), 10e-15)

    return df


def calculateScore(submissionFile, solutionFile):
    """
        Reads in submission file and solution file
        Calculates the logloss according to the competitions description
    """

    print("Reading submission file {}".format(submissionFile))
    submission = pd.read_csv(submissionFile, header=None)
    submission.columns = ["A", "B", "NEITHER"]

    submission = rescaleRows(submission)
    submission = replaceExtremes(submission)
    # print(submission.describe())

    print("Reading solution file {}".format(solutionFile))
    solution = pd.read_csv(solutionFile, index_col="ID", sep="\t")

    # print(solution.describe())

    aTerm = solution["A-coref"].astype(float).values[1600:] * np.log(
        submission["A"].values
    )
    bTerm = solution["B-coref"].astype(float).values[1600:] * np.log(
        submission["B"].values
    )
    neitherTerm = np.logical_not(
        np.logical_or(solution["A-coref"], solution["B-coref"])
    ).astype(float).values[1600:] * np.log(submission["NEITHER"].values)

    r = aTerm + bTerm + neitherTerm

    logloss = -np.mean(r)

    return logloss


def main():
    print("Calculating score of submission file")
    submissionFile = os.path.join(get_project_dir(), "data/predictions.csv")
    solutionFile = os.path.join(get_project_dir(), "data/gap-development.tsv")

    logloss = calculateScore(submissionFile, solutionFile)

    print("Logloss: {}".format(logloss))


if __name__ == "__main__":
    main()
