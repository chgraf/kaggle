"""
A script to download the data from kaggle or cernbox.
"""

import argparse
import requests
import os
import sys
import zipfile

from subprocess import CalledProcessError

from utils import run
from utils import get_project_dir


def main(args):
    if args.source == "kaggle":
        download_from_kaggle()
    elif args.source == "cernbox":
        if args.tag is None:
            print("Error: Must specify data tag when downloading from cernbox.")
            sys.exit(1)
        if not download_from_cernbox(tag=args.tag):
            print("Error: Could not download data")
            sys.exit(2)
    elif args.source == "GAP":
        download_from_GAPrepo()


def download_from_kaggle() -> bool:
    """Download the dataset from kaggle
    Downloaded files are stored in the data directory.
    """
    cwd = os.getcwd()
    os.chdir(os.path.join(get_project_dir(), "data"))
    try:
        run("kaggle competitions download -c gendered-pronoun-resolution")
    except CalledProcessError:
        print("Error: Could not download data from kaggle.")
        print("       Is you kaggle account setup?")
        print("       https://github.com/Kaggle/kaggle-api")
    unzip("test_stage_1.tsv.zip")
    os.chdir(cwd)

def download_from_GAPrepo() -> bool:
    """Download the dataset from the GAP Rep
    Downloaded files are stored in the data directory.
    """
    cwd = os.getcwd()
    os.chdir(os.path.join(get_project_dir(), "data"))
    try:
        run("wget https://raw.githubusercontent.com/google-research-datasets/gap-coreference/master/gap-development.tsv")
        run("wget https://raw.githubusercontent.com/google-research-datasets/gap-coreference/master/gap-validation.tsv")
        run("wget https://raw.githubusercontent.com/google-research-datasets/gap-coreference/master/gap-test.tsv")
    except CalledProcessError:
        print("Error: Could not download data from GAP repo")
    os.chdir(cwd)


def download_from_cernbox(tag: str) -> bool:
    """Download the dataset from cernbox.

    Downloaded files are stored in the data directory.

    Args:
        tag: Tag of the dataset that should be downloaded

    Returns:
        True if the dataset was downloaded and extracted, False otherwise
    """
    url_dict = {"1": "URL"}
    if tag not in url_dict:
        print(f"Error: Cernbox dataset tag {tag} is not supported.")
        return False
    dataset_name = f"cernbox_data_{tag}.zip"
    req = requests.get(url_dict[tag])
    with open(os.path.join(get_project_dir(), dataset_name), "wb") as f:
        f.write(req.content)

    return True


def unzip(fname: str) -> bool:
    """Unzip the contents of fname

    Args:
        fname: Filename of the .zip file

    Returns:
        True if unzipped successfully, False else
    """

    with zipfile.ZipFile(fname, "r") as zip_ref:
        zip_ref.extractall(os.path.join(os.path.splitext(fname)[0]))
    return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "source",
        default="kaggle",
        choices=["kaggle", "cernbox", "GAP"],
        help="Download the challenge data from kaggle or preprocessed data from cernbox",
    )
    parser.add_argument("--tag", "-t", help="Dataset tag (only for cernbox data")
    main(parser.parse_args())
