import numpy as np

# import spacy


class TextFeature:
    """ This class is used
    """

    def realtive_separation(self, sample: list) -> list:
        """ Returns the absolute difference between the pronouns
        and the proper-nous
        Args:
            One entry sample, not tokens
        Returns:
            Both location differences
        """

        return (
            np.abs(int(sample[3]) - int(sample[5])),
            np.abs(int(sample[3]) - int(sample[8])),
        )

    def children_count(self, sample_tokenized, A_token, B_token) -> list:
        """ Counts the total number of children of each of the
        target pronouns
        Args:
            Text, tokened
            Pronoun A, tokened
            Pronous B, tokened
        Return:
            Child count A,
            Child count B
        """

        A_counter = 0
        B_counter = 0

        for token in sample_tokenized:
            for child in token.children:
                for target in A_token:
                    if child.similarity(A_token) == 1:
                        if target.dep_ == "compound":
                            A_counter += 1
                        else:
                            A_counter += 1
                for target in B_token:
                    if child.similarity(B_token) == 1:
                        if target.dep_ == "compound":
                            B_counter += 1
                        else:
                            B_counter += 1
                for grand_child in child.children:
                    for target in A_token:
                        if grand_child.similarity(A_token) == 1:
                            if target.dep_ == "compound":
                                A_counter += 1
                            else:
                                A_counter -= 1
                    for target in B_token:
                        if grand_child.similarity(B_token) == 1:
                            if target.dep_ == "compound":
                                B_counter += 1
                            else:
                                B_counter -= 1

        return A_counter, B_counter

    def five_leading_words(self, sample_tokenized, pronoun_token) -> list:
        """ Calculated the leading 5 tokens before the pronoun
        Args:
            Text, token
            pronoun, token
        Return:
            List of tokens
        """
        words = []

        for t, token in enumerate(sample_tokenized):
            if token.similarity(pronoun_token) == 1:
                words.append(pronoun_token.similarity(sample_tokenized[t - 1]))
                words.append(pronoun_token.similarity(sample_tokenized[t - 2]))
                words.append(pronoun_token.similarity(sample_tokenized[t - 3]))
                words.append(pronoun_token.similarity(sample_tokenized[t - 4]))
                words.append(pronoun_token.similarity(sample_tokenized[t - 5]))
                break

        return words

    def extra_pronouns(self, sample_tokenized):
        """Get the number of pronouns in the sentence not including A and B"""
        return (
            len(
                [
                    entity
                    for entity in sample_tokenized.ents
                    if entity.label_ == "PERSON"
                ]
            )
            - 2
        )
