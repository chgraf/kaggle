import numpy as np
import h5py
import matplotlib.pyplot as plt

from keras.models import Model
from keras.layers import Dense, Input
from keras.layers.recurrent import LSTM
from keras.layers.merge import concatenate
from keras.optimizers import Adam

from sklearn.preprocessing import RobustScaler

# from keras.wrappers.scikit_learn import KerasRegressor
# from sklearn.model_selection import cross_val_score
# from sklearn.model_selection import KFold
# from sklearn.preprocessing import StandardScaler
# from sklearn.pipeline import Pipeline
# from keras.wrappers.scikit_learn import KerasClassifier

# import preprocessing

# This code is a silly example of how the preprocessing code can be used to train a simple MVA


def scale(X):

    transformer = RobustScaler().fit(X)
    return transformer.transform(X)


file = np.load("../data/training_data.npz")
X_train = scale(file["X_train"])
X_test = scale(file["X_test"])
R_train = file["R_train"]
R_test = file["R_test"]
y_train = file["y_train"]
y_test = file["y_test"]
l_train = file["l_train"]
l_test = file["l_test"]

print(np.shape(R_train))


def baseline_model():
    rnn_input = Input(shape=(5, 1), name="rnn_input")
    rnn_x = LSTM(10, activation="relu", return_sequences=True)(rnn_input)
    rnn_x = LSTM(10, activation="relu", return_sequences=True)(rnn_x)
    rnn_out = LSTM(10, activation="relu")(rnn_x)

    dnn_input = Input(shape=(X_train.shape[-1],), name="dnn_input")
    dnn_x = Dense(20, activation="relu")(dnn_input)
    dnn_x = Dense(20, activation="relu")(dnn_x)
    dnn_out = Dense(20, activation="relu")(dnn_x)

    merge = concatenate([rnn_out, dnn_out])

    x = Dense(20, activation="relu")(merge)
    x = Dense(20, activation="relu")(x)
    output = Dense(3, activation="softmax", name="output")(x)
    model = Model(inputs=[rnn_input, dnn_input], outputs=output)
    opt = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, decay=0.001)
    model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])
    model.summary()
    return model


# fix random seed for reproducibility
seed = 7
np.random.seed(seed)
model = baseline_model()

history = model.fit(
    [R_train, X_train], y_train, epochs=50, batch_size=20, validation_split=0.1
)
results = model.predict([R_test, X_test])

# save to csv
np.savetxt("../data/predictions.csv", results, delimiter=",")

output_file = h5py.File("../data/network_predictions.h5", "w")
output_file.create_dataset("predictions", data=results.flatten())
asciiList = [n.encode("ascii", "ignore") for n in l_test]
output_file.create_dataset("labels", data=asciiList)
# dset = output_file.create_dataset("init", data=l_test)


FALSEFALSETRUE = np.where(np.transpose(y_test)[0] == 1.0)[0]
FALSETRUEFALSE = np.where(np.transpose(y_test)[1] == 1.0)[0]
TRUEFALSEFALSE = np.where(np.transpose(y_test)[2] == 1.0)[0]
# print(y_test[:10])
# print(results[:10])
results_FFT = np.transpose(results)[0][FALSEFALSETRUE]
results_FTF = np.transpose(results)[1][FALSETRUEFALSE]
results_TFF = np.transpose(results)[2][TRUEFALSEFALSE]

plt.hist(results_FFT, alpha=0.5, bins="auto", linewidth=1.2)
plt.hist(results_FTF, alpha=0.5, bins="auto", linewidth=1.2)
plt.hist(results_TFF, alpha=0.5, bins="auto", linewidth=1.2)
plt.ylabel("Arbitrary units")
plt.xlabel("Probability")
plt.savefig("../data/response.png")
