import csv
import os

# import h5py
import spacy
import sys
import logging
from progress.bar import ShadyBar

# import pandas as pd
import numpy as np

from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder

# from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

import text_features
from utils import get_project_dir

logging.basicConfig(
    level=logging.INFO, format="[%(asctime)s %(levelname)s] %(message)s"
)


class DataPreparation:
    """Class that prepares and generates the competition data for
       model fitting
    """

    # This just loads the spacy nlp library, but any global variables should be added here
    def __init__(self):

        logging.info("Initiated")
        try:
            self.nlp = spacy.load("en_core_web_sm")
        except OSError:
            print("en_core_web_sm is not installed")
            print("Please run")
            print("  python -m spacy download en_core_web_sm")
            print("and try again. Exiting.")
            sys.exit(1)

    def get_labels(self) -> tuple:
        """The labels are extracted from the tsv with the solutions and creats a list
        of those labels. Assumes \t as separator. The format of those labels are:
        TRUEFALSEFALSE
        FALSETRUEFALSE
        FALSEFALSETRUE
        Args:
        -----
            file : Full path to the  .tsv that should be loaded. [default: gap-development.tsv in data dir]
        Returns:
            A list of labels for each entry in the tsv
            A list of the names of each sample
        """
        logging.info("Getting Labels")

        label_list = []

        sample_names = []

        with open(
            os.path.join(get_project_dir(), "data", "gap-development.tsv"), newline=""
        ) as csvfile:
            spamreader = csv.reader(csvfile, delimiter="\t")
            for i, row in enumerate(spamreader):
                if i == 0:

                    sample = ["A-corefB-corefNeither"]
                else:
                    sample = []
                    # for item in row:
                    sample_names.append(row[0])
                    # sample.append(row[6])
                    # sample.append(row[9])
                    if row[6] == "FALSE" and row[9] == "FALSE":
                        sample.append(row[6] + row[9] + "TRUE")
                    else:
                        sample.append(row[6] + row[9] + "FALSE")

                label_list.append(sample)

                # hf.create_dataset('dataset_1', data=l)
        # hf.close()
        # np.savez_compressed('../data/data.npz',data=datalist)
        return label_list, sample_names

    def get_data(
        self,
        input_file: str = os.path.join(
            get_project_dir(), "data", "gap-development.tsv"
        ),
    ) -> list:
        """The data is extracted from the kaggle tsv. Assumes \t as separator. Returns
        a list of the data The format of those labels are:
        Args:
            file: Full path to the  .tsv that should be loaded. [default: test_stage_1.tsv in data dir]
        Returns:
            A list of the data
        """
        logging.info("Getting data")

        datalist = []
        # hf = h5py.File('../data/data.h5', 'w')

        with open(input_file, newline="") as csvfile:
            spamreader = csv.reader(csvfile, delimiter="\t")
            for row in spamreader:

                datalist.append(row)

        return datalist

    # This returns the sample paragraph, the pronoun, and the two targets tokenized
    def _spacy_tokenizer(self, sample: list) -> list:
        """Returns the tokenized version of the data. This is the basic fucntionality
        of the function, anything that you want spaCY to extract to build a feature
        with should be added here.
        Args:
            One sample from the datalist, interated through in get_features [default: sample, list]
        Returns:
            Data paragraphs token,
            pronoun token,
            A_name token,
            B_name token
        """

        sample_tokenized = self.nlp(sample[1])
        pronoun_token = self.nlp(sample[2])
        A_token = self.nlp(sample[4])
        B_token = self.nlp(sample[6])
        token_vetos = self.nlp("( )")

        return sample_tokenized, A_token, B_token, pronoun_token, token_vetos

    # This builds the feature vector for each sample, the feature extractors are definied in a separte file
    def get_features(self, data: np.array) -> np.array:
        """ All features that are used for descrimination and/or training are
        created here and added to a featurelist
        Args:
            The list of data [default: data; generated from get_data() ]
        Returns:
            A numpy array of features, formated for keras training
        """
        bar = ShadyBar("Extracting features", max=len(data))

        dnn_featureslist = []
        rnn_featureslist = []

        feature = text_features.TextFeature()

        for i, sample in enumerate(data):

            # Uncomment the line below to access spaCY
            sample_tokenized, A_token, B_token, pronoun_token, token_vetos = self._spacy_tokenizer(
                sample
            )

            # First feature must declare the array
            dnn_sample_features = feature.realtive_separation(sample)
            rnn_sample_features = feature.five_leading_words(
                sample_tokenized, pronoun_token
            )

            # All other features much append
            # dnn_sample_features = np.append(
            #     dnn_sample_features,
            #     feature.children_count(sample_tokenized, A_token, B_token)
            # )

            dnn_sample_features = np.append(
                dnn_sample_features, feature.extra_pronouns(sample_tokenized)
            )
            # I've left this in for testing, helpful to see features' outputs
            # if i < 5:
            #    print(dnn_sample_features)
            #    print(rnn_sample_features)

            dnn_featureslist.append(dnn_sample_features)
            rnn_featureslist.append(rnn_sample_features)
            bar.next()

        return (
            np.reshape(
                np.array(dnn_featureslist),
                (len(dnn_featureslist), len(dnn_sample_features)),
            ),
            np.reshape(
                np.array(rnn_featureslist),
                (len(rnn_featureslist), len(rnn_sample_features), 1),
            ),
        )

    # Encodes the labeld as onehot encodes labels
    def encode_labels(self, labels: list) -> OneHotEncoder:
        """Takes the labels from get_labels and creates OneHot labels from them
        Args:
            list of labels [Default: label_list; from get_labels()]
        Returns:
            A list of OneHot encoded labels
        """
        logging.info("Encoding labels")

        # [0. 0. 1.]TRUEFALSEFALSE
        # [0. 1. 0.]FALSETRUEFALSE
        # [1. 0. 0.]FALSEFALSETRUE
        ## integer encode
        label_encoder = LabelEncoder()
        integer_encoded = label_encoder.fit_transform(labels)
        # binary encode
        onehot_encoder = OneHotEncoder(sparse=False)
        integer_encoded = integer_encoded.reshape(len(integer_encoded), 1)
        return onehot_encoder.fit_transform(integer_encoded)

    # Shuffles and splits the 3 arrays required for training and validation
    def make_training_data(
        self,
        dnn_features: np.array,
        rnn_features: np.array,
        sample_names: np.array,
        labels_encoded: np.array,
        train_number: int = 1600,
    ) -> list:
        """Creates training data for the silly_estimator.py script. This suffles
        and splits 3 different lists, featurs, sample_names, and labels_encoded.
        Args:
            A numpy array of features [default: features; returned from get_features()]
            A list of each sample name [default: sample names; returned from get_labels]
            A list of OneHotLables [default: labels_encoded; returned from get_labels]
        """
        logging.info("Creating training data")

        dnn_features, rnn_features, sample_names, labels_encoded = shuffle(
            dnn_features, rnn_features, sample_names, labels_encoded, random_state=1
        )
        X_train, X_test = dnn_features[:train_number], dnn_features[train_number:]
        R_train, R_test = rnn_features[:train_number], rnn_features[train_number:]
        y_train, y_test = labels_encoded[:train_number], labels_encoded[train_number:]
        l_train, l_test = sample_names[:train_number], sample_names[train_number:]
        # return train_test_split(features, labels, test_size=0.2, random_state=42)
        return X_train, X_test, R_train, R_test, y_train, y_test, l_train, l_test


if __name__ == "__main__":

    # The code below just tests all aspects of the class
    preprocessing = DataPreparation()
    data = preprocessing.get_data()
    labels, sample_names = preprocessing.get_labels()
    dnn_features, rnn_features = preprocessing.get_features(data[1:])
    labels_encoded = preprocessing.encode_labels(labels[1:])
    X_train, X_test, R_train, R_test, y_train, y_test, l_train, l_test = preprocessing.make_training_data(
        dnn_features, rnn_features, sample_names, labels_encoded
    )

    np.savez_compressed(
        os.path.join(get_project_dir(), "data", "training_data.npz"),
        X_train=X_train,
        X_test=X_test,
        R_train=R_train,
        R_test=R_test,
        y_train=y_train,
        y_test=y_test,
        l_train=l_train,
        l_test=l_test,
    )
